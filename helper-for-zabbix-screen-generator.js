
//This script generates part of another script ;)
//It can be used for populating arrays in zabbix-screen-generator.py
//
//In Zabbix navigate to screens -> constructor -> any of the "Change" links
// -> Select Graph -> new window will open
//In this new window
//1. choose non-jmx host from dropdown
//2. press F12 and paste the script into console
//3. Copy the output to python script
//4. select jmx host
//5. press up arrow to recall the script and run it again
//6. copy the output to python script
//(note that the script will generate different variable names
//for jmx and non-jmx hosts, so no need for any adjustments)

function toPythonArray(pyArrName, jsArray) {
   return pyArrName + " = ['" + jsArray.join("', '") + "']\n\n"
}

var exclude = new RegExp('Disk space usage|Number of AEM packages|Entropy level')

var hosts = [];
var graphsArr = [];
var output = "\n\n"

var hostsDropdown = document.querySelector('#hostid')
var selectedHost = hostsDropdown.options[hostsDropdown.selectedIndex].innerHTML;
var isHostJmx = selectedHost.endsWith('-jmx');
var hostsArrName = (isHostJmx) ? "hostsJmx" : "hosts";
var graphsArrName = (isHostJmx) ? "graphsJmx" : "graphs";


for (let i of $$('#hostid > option')) {
   let text = i.innerHTML;
   let isTextJmx = text.endsWith('-jmx');
   if (isHostJmx === isTextJmx) {
      hosts.push(text)
   }
}
for (let i of $$('#graphs tr a')) {
   let graphName = i.innerHTML;
   if (!exclude.test(graphName)) {
      graphsArr.push(graphName);
   }
}
output += toPythonArray(hostsArrName, hosts);
output += toPythonArray(graphsArrName, graphsArr);

console.info(output);
