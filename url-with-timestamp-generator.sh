#! /bin/bash

#these are just examples, modify the values to reflect your domains/settings
zabbixUrl="https://zabbix.yourdomain.com/zabbix/screens.php?elementid=23"
grafanaUrl="http://127.0.0.1:3000/dashboard/db/jmeter-perf"

#assign 0 for no shift
#you can assign negative values too
zabbixShiftInMinutes=-60
grafanaShiftInMinutes=0

browserPath='C:/path/to/chrome.exe'

#######################################
#### do NOT modify below this line ####

openInBrowser=false

printUsageAndExit() {
    echo -e "\nUsage: $(basename $0) jmeterResultCsv.jtl [-o|--open]\n"
    exit -1
}

if [ $# -eq 0 ]; then
    printUsageAndExit;
fi

if [ ! -f $1 ]; then
    echo "No such file: $1"
    printUsageAndExit;
fi

if [ "$2" == "-o" ] || [ "$2" == "--open" ]; then
    if [ -f "$browserPath" ]; then
        openInBrowser=true
    else
        echo "WARNING! No such file: $browserPath"
        echo "--open option will have no effect. Please set browserPath variable in the script."
    fi
fi


jtlFile=$1
startTimeMs=$(head -n2 $jtlFile | tail -n1 | cut -d, -f1)
endTimeMs=$(tail -n1000 $jtlFile | cut -d, -f1 | sort | tail -n1) #end of jtl file can get messy and the latest line is not always at the end

#grafana part
grafanaShiftInMilliseconds=$(($grafanaShiftInMinutes*60000))
grafanaFrom=$(($startTimeMs+$grafanaShiftInMilliseconds))
grafanaTo=$(($endTimeMs+$grafanaShiftInMilliseconds))
grafanaOut="$grafanaUrl?from=$grafanaFrom&to=$grafanaTo&refresh=1d"


#zabbix part
zabbixShiftInSeconds=$(($zabbixShiftInMinutes*60))
startTimeInSeconds=$((($startTimeMs+500)/1000))
startTimeShifted=$(($startTimeInSeconds+$zabbixShiftInSeconds))

durationMs=$(($endTimeMs-$startTimeMs))
durationInSeconds=$((($durationMs+500)/1000))

zabbixStime=$(date -d @$startTimeShifted +%Y%m%d%H%M%S)
zabbixOut="$zabbixUrl&stime=$zabbixStime&period=$durationInSeconds"


#print both urls
echo
echo $grafanaOut
echo
echo $zabbixOut
echo
echo "WARNING: due to a bug in zabbix you need to manually select the entire period (from start to end) on any graph/picture on a screen for the times to be displayed correctly"
echo

if [ $openInBrowser == true ]; then
    "$browserPath" "$grafanaOut" "$zabbixOut"
fi

