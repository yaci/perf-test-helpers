#! /bin/bash

# you can override console input and provide all the parameters here, e.g.
# args="-v 3.3 -h 4096 -p jpgc-graphs-basic -p jpgc-graphs-additional -p jpgc-graphs-composite -p jpgc-graphs-dist -p jpgc-graphs-vs -p jpgc-synthesis -p jpgc-standard"

args="$@"

# Urls should be changed only if necessary
# IMPORTANT: all urls must be surrounded by SINGLE quotes (') NOT double (")
# The reason for this is that we delay parameter expansion, i.e $jmeterVersion
# is evaluated later in getParametersFromCmd() function
jmeterDownloadUrl='http://www-eu.apache.org/dist/jmeter/binaries/apache-jmeter-$jmeterVersion.tgz'
jmeterChecksumUrl='https://www.apache.org/dist/jmeter/binaries/apache-jmeter-$jmeterVersion.tgz.sha512'
pluginManagerUrl='https://jmeter-plugins.org/get/'
cmdLinePluginUrl='http://search.maven.org/remotecontent?filepath=kg/apc/cmdrunner/2.0/cmdrunner-2.0.jar'



##################################################
### try not to modify anything below this line ###

jmeterVersion=
heapSizeInMB=
pluginsToBeInstalled=

usage() {
   echo
   echo "Usage: $0 -v jmeter_version -h heap_size [-p plugin1 -p plugin2 -p plugin3...]"
   echo
   echo "Parameters explanataion:"
   echo
   echo "  -v jmeter version"
   echo "         JMeter version to be installed. This argument is obligatory."
   echo
   echo "  -h heap size in megabytes (number)"
   echo "         Files jmeter.bat, jmeter and jmeter.sh will be amended so"
   echo "         when run, JMeter will start with enough memory assigned."
   echo "         This argument is obligatory."
   echo
   echo "  -p plugin name"
   echo "         Plugin to be installed. You can use this parameter multiple times"
   echo "         This argument is optional"
   echo
   echo "Examples:"
   echo
   echo "  $0 -v 3.3 -h 2048"
   echo "         This will install JMeter version 3.3 and JMeter will be allowed to use up to 2048MB of memory"
   echo
   echo "  $0 -v 3.2 -h 4096 -p jpgc-graphs-basic -p jpgc-standard -p jpgc-synthesis"
   echo "         This will install JMeter 3.2 and three plugins, heap size will be set to 4096MB"
   echo
   exit 1;
}

getParametersFromCmd(){
   unset -v jmeterVersion heapSize pluginsToBeInstalled   
   while getopts ":v:h:p:" o; do
      case "${o}" in
         v)
            jmeterVersion=${OPTARG} ;;
         h)
            heapSizeInMB=${OPTARG} ;;
         p)
            pluginsToBeInstalled+=("$OPTARG") ;;
         *)
            usage ;;
      esac
   done
   shift $((OPTIND-1))

   if [ -z "${jmeterVersion}" ] || [ -z "${heapSizeInMB}" ]; then
      usage
   fi
   
   eval "jmeterDownloadUrl=\"$jmeterDownloadUrl\""
   eval "jmeterChecksumUrl=\"$jmeterChecksumUrl\""
   eval "pluginManagerUrl=\"$pluginManagerUrl\""
   eval "cmdLinePluginUrl=\"$cmdLinePluginUrl\""
}


printMessageAndExit() {
   echo $1
   echo "Exiting..."
   exit -1
}

commandExists() {
   command -v "$1" >/dev/null 2>&1
}

installPackage() {
   if commandExists $1 ; then
      return
   fi
   pkgManagers=( yum apt-get brew pkg dnf pact apt-cyg choco )
   for manager in "${arrayName[@]}"; do
      if commandExists $manager ; then
         $manager install $1
         if [ $? -eq 0 ]; then
            return
         else
            echo "Failed to install $1"
         fi
      fi
   done
   echo "Cannot install $1. No package manager available..."
}

verifyHashSum() {
   echo "Verifying checksum of $1"
   if commandExists sha512sum ; then
      sha512sum -c "$1"
   else
      shasum -a 512 -c "$1"
   fi
   if [ $? -ne 0 ]; then
      printMessageAndExit "Cheksum does not match!!"
   fi
}

downloadFile() {
   echo Downloading "$1" from "$2"
   if commandExists wget ; then
      wget --quiet "$2"
   else
      curl --silent --remote-name --location "$2"
   fi
}

# The problem with plugins is that we don't know the name of the file
# before it's downloaded.
# --remote-header-name and --content-disposition options for curl and wget
# respectively attempt to retrieve the file name form http header and save
# it under that name. Without them the name is incorrectly derived from url.
# Reportedly --remote-header-name does not have expected effect on MacOS
# and that is why we are desperately trying to use wget if possible.
downloadPlugin() {
   echo Downloading plugin "$1" form "$2" to "$3"
   if commandExists wget ; then
      wget --quiet --content-disposition "$2" -P "$3"
   else
      (cd "$3"; curl --silent --remote-name --remote-header-name --location "$2")
   fi
   if [ $? -ne 0 ]; then
      echo "Download failed!!"
   fi
}

############################################

getParametersFromCmd $args
installPackage wget
mkdir jmeter

##download and unpack jmeter##

downloadFile JMeter "$jmeterDownloadUrl"
downloadFile jmeter-hash "$jmeterChecksumUrl"
verifyHashSum apache-jmeter-${jmeterVersion}.tgz.sha512
echo "Unpacking JMeter..."
tar -xf apache-jmeter-$jmeterVersion.tgz -C ./jmeter --strip-components 1

##amend jmeter run files to modify heap size##

echo "Setting heap size in JMeter run files..."
heapPattern="HEAP[[:blank:]]*=[[:blank:]]*\"*-Xms[[:digit:]]*m -Xmx[[:digit:]]*m\"*"

# The only difference between Unix and Windows settings is that the Unix one
# is surrounded with quotes (""). To avoid specifying (almost) the same
# string twice, we remove the quotes automatically. If, however, for any
# reason the Windows version should significantly differ - just assign
# correct value to the appropriate variable
heapArgsForJMeterUnix="HEAP=\"-Xms${heapSizeInMB}m -Xmx${heapSizeInMB}m\""
heapArgsForJMeterWindows=$(echo $heapArgsForJMeterUnix | sed 's/"//g')

sed -i "s/^${heapPattern}/${heapArgsForJMeterUnix}/" jmeter/bin/jmeter
sed -i "/^java/ i ${heapArgsForJMeterUnix}" jmeter/bin/jmeter.sh
sed -i "s/^java/java \$HEAP/" jmeter/bin/jmeter.sh
sed -i "s/^set ${heapPattern}/set ${heapArgsForJMeterWindows}/" jmeter/bin/jmeter.bat

##download and install plugins##

downloadPlugin 'PluginManager' "$pluginManagerUrl" 'jmeter/lib/ext'
downloadPlugin 'CMD Plugin'    "$cmdLinePluginUrl" 'jmeter/lib'

# generate plugin installers (.sh and .bat files)
java -cp jmeter/lib/ext/jmeter-plugins-manager-*.jar org.jmeterplugins.repository.PluginManagerCMDInstaller
# specify custom logging configuration for jmeter-plugins' java code to suppress "garbage" logs
export JVM_ARGS='-Dlog4j.configurationFile=./log4j2plugins.xml'
for plugin in ${pluginsToBeInstalled[@]}; do
   bash ./jmeter/bin/PluginsManagerCMD.sh install $plugin
done

echo -e "\n\n DONE!\n\n"
