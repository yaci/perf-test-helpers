#! /bin/bash
JMETER_FILE='c:/dev/apache-jmeter-5.2/bin/jmeter.sh'
REPORT_GRANULARITIES_IN_SECONDS=(1 15 30 60)
REPORT_TITLE="YOUR_NAME_HERE perfromance results, generated on $(date +%F)"

## try not to modify anything below this line ##
#### (except line 40 - you MUST modify it!! ####

REPORT_DIR="report-$(date +%Y%m%d-%H%M)"
GRAPHS_DIR="$REPORT_DIR"
GRAPHS_FILTERED_DIR="$REPORT_DIR/noEmbeddedResources"
LOG_FILE="$1"
TEST_FILE="$2"
PREFIX='jmeter.reportgenerator'
LOG_NAME=$(basename "$LOG_FILE")
FILTERED_LOG="$REPORT_DIR/noEmbeddedResources-${LOG_NAME}"

printHelp () {
   echo -e "\n Usage: ./generateReport.sh <LOG_FILE.jtl> <TEST_FILE.jmx>\n\n [file3 file4 ...]"
   exit 1
}

function makeReports() {
   CURRENT_LOG_FILE="$1"
   CURRENT_REPORT_DIR="$2"
   for granularity in "${REPORT_GRANULARITIES_IN_SECONDS[@]}"
   do
      workingDir="$CURRENT_REPORT_DIR/report-granularity-${granularity}s"
      granularityInMillis=$(($granularity*1000))
      mkdir -p "$workingDir" || (echo "Cannot create $workingDir" && exit 6)
      "$JMETER_FILE" -g "$CURRENT_LOG_FILE" -o $workingDir -J${PREFIX}.report_title="$REPORT_TITLE" -J${PREFIX}.overall_granularity=$granularityInMillis
      echo "Generated: $workingDir"
  done
}

function filterOutEmbeddedResourcesAndDetermineWhetherToCreateAdditionalVersionOfReport() {
   #copy the csv header (first line)
   head -n 1 $NEW_LOG_FILE > "$FILTERED_LOG"
   #filter out embedded resources
   cat $NEW_LOG_FILE | grep 'somedomain.net/en-gb' | grep -v '\-0' >> "${FILTERED_LOG}" 
   
   diff -q "$NEW_LOG_FILE" "$FILTERED_LOG"
   if [[ $? -eq 0 ]]; then
       rm "$FILTERED_LOG"  #files are the same
       echo "No embedded resources detected - assuming only one version of report"
   else
       GRAPHS_DIR="${REPORT_DIR}/embeddedResourcesIncluded"
       echo "Separate report will be created with embedded resources filtered out (in addition to original one)"
   fi   
}



[ "$2" == "" ] && printHelp

[ ! -f "$JMETER_FILE" ] && echo -e "\nJMeter not found in $JMETER_FILE\n"               && exit 2
[ ! -f "$LOG_FILE" ]    && echo -e "\nLog file (jtl file) $LOG_FILE does not exist\n"   && exit 3
[ ! -f "$TEST_FILE" ]   && echo -e "\nTest file (jmx file) $TEST_FILE does not exist\n" && exit 4

mkdir "$REPORT_DIR" || exit 5
NEW_LOG_FILE="${REPORT_DIR}/$(basename $LOG_FILE)"

#remove 100 last lines from log
#(1 line has to be removed anyway to fix a case
# where test run was interrupted with ctrl+c)
head -n -100 $LOG_FILE > $NEW_LOG_FILE

#if there's a need to create additional report an extra file will be left on disk
#in other words: this function does not return status, the further behaviour is
#determined by presence (or absence) of the file
filterOutEmbeddedResourcesAndDetermineWhetherToCreateAdditionalVersionOfReport

makeReports "$NEW_LOG_FILE" "$GRAPHS_DIR"
[[ -f "$FILTERED_LOG" ]] && makeReports "$FILTERED_LOG" "$GRAPHS_FILTERED_DIR"



cp "$TEST_FILE" "$REPORT_DIR"
[[ ! -z "$3" ]] && cp "$3" "$REPORT_DIR"
[[ ! -z "$4" ]] && cp "$4" "$REPORT_DIR"

tar cfz ${REPORT_DIR}.tar.gz $REPORT_DIR

echo -e "\n  DONE!\n\n"
