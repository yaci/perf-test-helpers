#! /bin/bash

JAVA_PID=$(pgrep java)

if [ ! "$JAVA_PID" == "" ]; then
   dateHeader=$(date +"----%T (%d)----")
   ssResult=$(/usr/sbin/ss -s)
   descriptors=$(sudo ls /proc/$(pgrep java)/fd | wc -l)

   echo -e "$dateHeader\n\n$ssResult\n\ndesc: $descriptors\n\n\n" >> /home/user/dir/stats.txt
fi

# add the scritpt to cron:
# crontab -e
# * * * * * /home/user/dir/descriptors-monitor.sh
