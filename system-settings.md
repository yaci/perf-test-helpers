If JMeter shows signs of opening so many connections that the system cannot handle them, try the tweaks below. Note that some of the settings are not permanent, i.e. will reset to default after restart, so you may want to make appropriate entries in .bashrc

1. Increase the limit of open file descriptors
  * `sudo vim /etc/security/limits.conf` and enter the following to lines into the file
       * `hard nofile 50000`
       * `soft nofile 50000`

2. Allow the system to reuse connections in time_wait state
  * `sudo sysctl -w net.ipv4.tcp_tw_reuse=1`
  * `sudo sysctl -w net.ipv4.tcp_tw_recycle=1`

3. Increase number of ports (port range) that can be allocated by JMeter
  * `sudo sysctl -n net.ipv4.ip_local_port_range="2000   65535"`
