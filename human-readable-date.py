# This is an alternative version to bash/perl human-redable-date
# One should use the other (perl) one, because it's noticably faster.
# This version however gives slightly more control over the process.

import sys
import re
from datetime import datetime

def repFunc(matchobj):
    return datetime.fromtimestamp(int(matchobj.group(0)) / 1000).strftime("%Y-%m-%d %H:%M:%S")

numbers = re.compile("^\d{13}")
for line in sys.stdin:
    sys.stdout.write(numbers.sub(repFunc, line))
