import xml.etree.ElementTree as ET

# Instead of manually populating arrays one should use a script -
# look for helper-for-zabbix-screen-generator.js file
hosts = ['proj-perf-publish-aem1', 'proj-perf-publish-aem2', 'proj-perf-publish-aem3', 'proj-perf-publish-aem4', 'proj-perf-publish-dispatcher1', 'proj-perf-publish-dispatcher2', 'proj-perf-publish-dispatcher3', 'proj-perf-publish-dispatcher4', 'proj-perf-publish-knotx1', 'proj-perf-publish-knotx2', 'proj-perf-publish-knotx3', 'proj-perf-publish-knotx4', 'proj-perf-publish-origin1', 'proj-perf-publish-origin2', 'proj-perf-publish-origin3', 'proj-perf-publish-origin4']
graphs = ['CPU jumps', 'CPU load', 'CPU utilization', 'Disk xvda statistics - Bytes per sec', 'Disk xvda statistics - ms', 'Disk xvda statistics - Ops per sec', 'Disk xvdb statistics - Bytes per sec', 'Disk xvdb statistics - ms', 'Disk xvdb statistics - Ops per sec', 'Memory statistics', 'Memory usage', 'Network traffic on eth0', 'Swap usage']

hostsJmx = ['proj-perf-author-aem1-jmx', 'proj-perf-author-knotx1-jmx', 'proj-perf-publish-aem1-jmx', 'proj-perf-publish-aem2-jmx', 'proj-perf-publish-aem3-jmx', 'proj-perf-publish-aem4-jmx', 'proj-perf-publish-knotx1-jmx', 'proj-perf-publish-knotx2-jmx', 'proj-perf-publish-knotx3-jmx', 'proj-perf-publish-knotx4-jmx']
graphsJmx = ['JMX Garbage Collector collections per second', 'JMX Memory Pool Code Cache', 'JMX Memory Pool Compressed Class Space', 'JMX Memory Pool Metaspace']

hostsJmxSet = frozenset(hostsJmx);

def addElement(xmlParent, tagName, value):
    ET.SubElement(xmlParent, tagName).text = str(value)

def addGraph(x, y, graphName, host, xmlScreenItems):
    si = ET.SubElement(xmlScreenItems, "screen_item")
    resource = ET.SubElement(si, "resource")
    addElement(resource, "name", graphName)
    addElement(resource, "host", host)
    addElement(si, "x", x)
    addElement(si, "y", y)
    addElement(si, "resourcetype", "0")
    addElement(si, "width", "500")
    addElement(si, "height", "100")
    addElement(si, "colspan", "1")
    addElement(si, "rowspan", "1")
    addElement(si, "elements", "0")
    addElement(si, "valign", "0")
    addElement(si, "halign", "0")
    addElement(si, "style", "0")
    addElement(si, "url", "")
    addElement(si, "dynamic", "0")
    addElement(si, "sort_triggers", "0")
    addElement(si, "max_columns", "3")
    addElement(si, "application", "")
    
def main():
    xSize = len(hosts)
    ySize = len(graphs) + len(graphsJmx)
    root = ET.Element("zabbix_export")
    addElement(root, "version", "3.2")
    screens = ET.SubElement(root, "screens")
    screen = ET.SubElement(screens, "screen")
    addElement(screen, "name", "cokolwiek")
    addElement(screen, "hsize", xSize)
    addElement(screen, "vsize", ySize)
    screenItems = ET.SubElement(screen, "screen_items")
    for x in range(len(hosts)):
        for y in range(len(graphs)):
            addGraph(x, y, graphs[y], hosts[x], screenItems)
        jmxName = hosts[x] + "-jmx";    
        if (jmxName in hostsJmxSet):
            for yy in range(len(graphsJmx)):
                addGraph(x, y + yy + 1, graphsJmx[yy], jmxName, screenItems)
    print ET.tostring(root, encoding="utf-8", method="xml")    
    

if __name__ == '__main__':
    main()