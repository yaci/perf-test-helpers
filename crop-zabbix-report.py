# you need to install BeautifulSoup for the script to work -
# pip install BeautifulSoup
import sys
try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup


htmlFile = sys.argv[1]
str = open(htmlFile, 'r').read()
soup = BeautifulSoup(str)
soup.find('header').decompose()
soup.find('div', id='msg-bad-global').decompose()
soup.find('div', id='zbx_messages').decompose()
soup.find('div', attrs={'class':'footer'}).decompose()
article = soup.find('div', attrs={'class':'article'})

for elem in article.findChildren(recursive=False):
    if "table-forms-container" not in elem['class']:
        elem.decompose()

for elem in soup.findAll('script'):
    elem.decompose()

print soup